#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QVBoxLayout>
#include <QFileDialog>
#include <QMessageBox>
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    tabWidget = new QTabWidget(this);
    tabWidget->setMovable(true);
    tabWidget->setTabsClosable(true);
    on_actionNew_triggered();

    setCentralWidget(tabWidget);

    connect(tabWidget,SIGNAL(currentChanged(int)),this,SLOT(tabSelected()));
    connect(tabWidget, SIGNAL(tabCloseRequested(int)), this, SLOT(closeTab(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionNew_triggered()
{

    QWidget *widget = new QWidget(this);
    widgetList << widget;
    tabWidget->addTab(widget, QString("New%1").arg(widgetList.size()));

    QTextEdit *textEdit =  new QTextEdit;
    textEditList << textEdit;
    QVBoxLayout* vBoxLayout = new QVBoxLayout(widget);
    vBoxLayout->addWidget(textEdit);

    index = tabWidget->currentIndex();

}

void MainWindow::on_actionSaveAs_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,tr("Saving file ..."),"*.txt",tr("All Files(*)"));

    QString text = textEditList.at(index)->toPlainText();
    QStringList strList = text.split(QRegExp("[\n]"),QString::SkipEmptyParts);
    QString text2;
    foreach (QString line, strList)
    {
        text2 = text2 + line + '\n';
    }


    tabWidget->currentWidget()->setWindowTitle(fileName);


    QFile f(fileName);
    f.open(QIODevice::WriteOnly);
    QTextStream stream(&f);
    stream<<QString(text2);
    f.close();


}

void MainWindow::tabSelected()
{
    index = tabWidget->currentIndex();
}

void MainWindow::on_actionOpen_triggered()
{

    QString fileName1 = QFileDialog::getOpenFileName(this,tr("Open Text File"), "", tr("Text Files (*.txt)"));
    path = fileName1;
    QStringList strList = fileName1.split(QRegExp("[/]"),QString::SkipEmptyParts);

    QString name = strList.at(strList.size()-1);

    QFile file1(fileName1);
    if(!file1.open(QIODevice::ReadOnly | QIODevice::Text))
        return;

    QTextStream in(&file1);
    QString line;
    while(!in.atEnd())
    {
        line += in.readLine();
    }


    QWidget *widget = new QWidget(this);
    widgetList << widget;
    tabWidget->addTab(widget, (QString("%1").arg(name)));

    QTextEdit *textEdit =  new QTextEdit;
    textEditList << textEdit;
    QVBoxLayout* vBoxLayout = new QVBoxLayout(widget);
    vBoxLayout->addWidget(textEdit);
    index = tabWidget->currentIndex();

    textEdit->document()->setPlainText(line);

}

void MainWindow::on_actionSave_triggered()
{
    QString text = tabWidget->tabText(index);

    if(text.contains("New") == true)
    {
        on_actionSaveAs_triggered();
    }
    else
    {
        QString  content = textEditList.at(index)->toPlainText();
        QFile f(path);
        f.open(QIODevice::WriteOnly);
        QTextStream stream(&f);
        stream<<QString(content);
        f.close();
    }
}

void MainWindow::closeTab(int index)
{

    tabWidget->removeTab(index);
    delete tabWidget->widget(index);
}
