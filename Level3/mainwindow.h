#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextEdit>
#include <QtCore>
#include <QMouseEvent>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionNew_triggered();

    void on_actionSaveAs_triggered();

    void tabSelected();
    void on_actionOpen_triggered();

    void on_actionSave_triggered();

    void closeTab(int index);
private:
    Ui::MainWindow *ui;
    QList<QWidget*> widgetList;
    QList<QTextEdit*> textEditList;
    QTabWidget* tabWidget;
    int index;
    QString path;
};

#endif // MAINWINDOW_H
