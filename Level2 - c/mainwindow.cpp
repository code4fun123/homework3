#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QLineEdit>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->addBtn,SIGNAL(clicked()),this,SLOT(addClick()));

    number = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addClick()
{
    if(number<5)
    {
        QLineEdit * button = new QLineEdit;
        ui->verticalLayout->addWidget(button);
        button->show();
        number++;
        if(number==5)
        {
            ui->addBtn->setDisabled(true);
        }
    }

}
