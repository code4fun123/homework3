#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<iostream>
#include<QMessageBox>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->button,SIGNAL(clicked()),this,SLOT(copyClicked()));

    connect(ui->button_2,SIGNAL(clicked()),this,SLOT(clearClick()));

}

MainWindow::~MainWindow()
{
    delete ui;



}

void MainWindow::copyClicked()
{

    QString text = ui->field1->text();
    if(text=="")
    {
       QMessageBox::information(this,tr("Error!"),tr("Empty field!"));
    }
    else
    {
         ui->field2->setText(text);
    }

}

void MainWindow::clearClick()
{
    ui->field1->setText("");
    ui->field2->setText("");

}
