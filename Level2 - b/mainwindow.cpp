#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    red = 0;
    green = 0;
    blue = 0;

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_slider1_valueChanged(int value)
{
    red = value;
    ui->centralWidget->setStyleSheet(QString("background:rgb(%1,%2,%3);").arg(red).arg(green).arg(blue));
}


void MainWindow::on_slider2_valueChanged(int value)
{
    green = value;
    ui->centralWidget->setStyleSheet(QString("background:rgb(%1,%2,%3);").arg(red).arg(green).arg(blue));
}


void MainWindow::on_slider3_valueChanged(int value)
{
    blue = value;
    ui->centralWidget->setStyleSheet(QString("background:rgb(%1,%2,%3);").arg(red).arg(green).arg(blue));
}
